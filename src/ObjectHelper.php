<?php

namespace helpers;

use helpers\exceptions\IncorrectPathException;
use helpers\exceptions\NullObjectException;

/**
 * Class ObjectHelper
 * @package helpers
 */
class ObjectHelper
{

    public function __construct($full_class_name)
    {
        $path = explode("\\", $full_class_name);
        $this->class_name = \array_pop($path);
        $this->namespace = \implode("\\", $path) . "\\";
    }

    /**
     * @param object $object
     * @param string $path in format 'property->property[0]->property'
     * @return mixed|null
     * @throws IncorrectPathException
     * @throws NullObjectException
     */
    public static function getObjectByPath($object, $path, $can_be_null = false)
    {
        $root_object = $object;
        $array_path = explode('->', $path);
        foreach ($array_path as $step) {
            if ($step == '') {
                throw new IncorrectPathException('Path "' . $path . '" has empty parts');
            }
            list('name' => $name, 'index' => $index) = ObjectHelper::parseIndex($step);

            $object = $object->$name ?? null;
            if ($index == 'last') {
                $object = \reset($object);
            } elseif ($index == 'last') {
                $object = \end($object);
            } elseif ($index !== null) {
                $object = $object[$index];
            }
            $object = (array)$object == [] ? null : $object;
            if ($object === null) {
                if ($can_be_null) {
                    return null;
                } else {
                    throw new NullObjectException('Not fount element by path "' . $path . '" in object ' . \var_export($root_object));
                }
            }
        }
        return $object;
    }

    /**
     * @param $str
     * @return ['name' => string, 'index' => string]
     */
    protected static function parseIndex($str)
    {
        $pos = strpos($str, '[');
        if ($pos) {
            $index = substr($str, $pos + 1, -1);
            $step = substr($str, 0, $pos);
        } else {
            $index = null;
            $step = $str;
        }

        return ['name' => $step, 'index' => $index];
    }
}